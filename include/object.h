#ifndef OBJECT_H
#define OBJECT_H

#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

extern const void * Object;		/* new(Object); */
extern const void * Class;	/* new(Class, "name", super, size, sel, meth, ... 0); */

void * new (const void * class, ...);
void delete (void * self);

const void * classOf (const void * self);
size_t sizeOf (const void * self);
const void * super (const void * self);	/* class' superclass */

#endif
