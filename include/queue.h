#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>
#include <assert.h>

extern const void * Queue;       /* new(Queue, buff_size) */
extern const void * QueueClass; /* adds write, read */

int buff_capacity(const void * self);
int buff_avail(const void * self);
void * buff_write(const void * self, void * msg);
void * buff_read(const void * self);
int buff_flush(const void * self);

#endif
