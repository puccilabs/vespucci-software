###
### Configureables
###

# Main program prefix
MAIN			?=	main

# Compiler
CC				?= ${ARM}
ARM				= arm-none-eabi-gcc
GCC				= gcc

# Compiler options
CFLAGS			= -Wall -pedantic --std=c11
GCC_FLAGS		= $(CFLAGS)
ARM_FLAGS		= $(CFLAGS) --specs=nosys.specs
DEBUG_FLAGS		=

# Directory information
BASE_DIR		:= .
BIN_DIR			:= ${BASE_DIR}/bin
BUILD_DIR		:= ${BASE_DIR}/build
INCLUDE_DIR		:= ${BASE_DIR}/include
SRC				:= ${BASE_DIR}/src
SRC_DIRS		:= ${sort ${dir ${wildcard ${SRC}/*/}}}
SRCS			:= ${wildcard ${SRC_DIRS}*.c}
DEPS			:= ${foreach FILE,${notdir ${SRCS:.c=.d}},${BUILD_DIR}/FILE}
BINS			:= ${notdir ${SRCS:.c=.o}}
VPATH			:= ${SRC_DIRS}

# Header includes
INCLUDE_H			= -I${INCLUDE_DIR}/
INCLUDE_H			+= ${subst ${BASE_DIR},-I${BASE_DIR}, ${SRC_DIRS}}

###
### Standard (Phony) Targets
###

.PHONY: default debug compile link install clean
default: clean compile link
debug: default
	DEBUG_FLAGS += -g
#compile: ${BIN_DIR}/main.o
compile: ${BINS}
link: ${BIN_DIR}/${MAIN}
install:
clean:
	rm -f ${BIN_DIR}/*

###
### Target Rules
###

# Compile all
%.o: %.c
	${CC} ${DEBUG_FLAGS} ${ARM_FLAGS} ${INCLUDE_H} -MMD -MF ${BUILD_DIR}/${notdir ${basename $@}}.d -o ${BIN_DIR}/$@ -c $<

# Link main program
${BIN_DIR}/${MAIN}: ${MAIN}.o
	${CC} ${DEBUG_FLAGS} ${ARM_FLAGS} ${INCLUDE} -o $@ $<

# Test compile
${BIN_DIR}/main.o: main.c
	${CC} --specs=nosys.specs -c -o $@ $<

-include ${DEPS}