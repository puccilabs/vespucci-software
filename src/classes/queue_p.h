#ifndef QUEUE_P
#define QUEUE_P

#include "object_p.h"

struct Queue
{
    const struct Object _;
    void * buff;
};

struct QueueClass
{
    const struct Class _;
    void * (*buff_write)(void * msg);
    void * (*buff_read)(void *);
};

#endif
