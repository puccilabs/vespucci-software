#include "queue.h"
#include "queue_p.h"

static void * Queue_ctor(void * _self, va_list * app)
{
    struct Queue * self = _self;
    const int buff_len = va_arg(* app, const int);
    self->buff = calloc(1, buff_len);
    assert(self->buff);
    return self;
}

static void * Queue_dtor(void * _self)
{
    struct Queue * self = _self;
    free(self->buff);
    self->buff = 0;
    return self;
}

const void * QueueClass;
const void * Queue;

void initQueue(void)
{
    
}