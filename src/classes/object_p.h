#ifndef OBJECT_P
#define OBJECT_P

#include <stddef.h>
#include <stdarg.h>

struct Object {
	const struct Class * class;	/* object's description */
};

struct Class {
	const struct Object _;			/* class' description */
	const char * name;				/* class' name */
	const struct Class * super;		/* class' super class */
	size_t size;					/* class' object's size */
	void * (* ctor) (void * self, va_list * app);
	void * (* dtor) (void * self);
};

void * super_ctor (const void * class, void * self, va_list * app);
void * super_dtor (const void * class, void * self);

#endif
