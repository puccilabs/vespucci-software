import logging
from logging import NullHandler

from pyftdi.ftdi import Ftdi
from pyftdi.serialext import serial_for_url
from pyftdi.usbtools import UsbDeviceDescriptor as TypeUSB

log = logging.getLogger(__name__)
log.addHandler(NullHandler())


class Programmer():
    def __init__(self):
        self._ftdi = Ftdi()
        self._serial = None
        self.connected = None
        self.vid = Ftdi.FTDI_VENDOR
        self.pid = Ftdi.PRODUCT_IDS[self.vid]['231x']
        self._bus_pin = 3
        self._pwr_pin = 0
        self._cbus_init_state = 0b0000
        self._cbus_state = self._cbus_init_state
        self.device_sn = self.scanDevices()

    def __del__(self):
        if self.connected:
            self.disconnect()

    def connect(self, sn:str, baud:int):
        if sn in self.device_sn:
            try:
                dev_id = 'ftdi://::' + sn + '/1'
                self._ftdi.open_from_url('ftdi://::' + sn + '/1')
                self._serial = serial_for_url(dev_id, baudrate=baud, timeout=0)
                self._initCBus()
                self.connected = sn
                log.info('Connected to device: {}'.format(self.connected))
            except Exception as e:
                log.warning(e)
                self.__init__()
        else:
            log.warning('Scan for devices prior to connecting.')

    def disconnect(self):
        if self.connected:
            self.flushBuffers()
            self._initCBus()
            self._serial.close()
            self._ftdi.close()
            self.connected = None
        else:
            log.warning('Connect to a device prior to disconnecting.')

    def flushBuffers(self):
        if self.connected:
            self._serial.reset_output_buffer()
            self._serial.reset_input_buffer()
        else:
            log.warning('Connect to a device prior to flushing buffers.')

    def rx(self, num_bytes):
        if self.connected:
            return self._read(num_bytes)
        else:
            log.warning('Connect to a device prior to receiving data.')

    def scanDevices(self):
        self.devices = self._ftdi.find_all([(self.vid,self.pid)])
        attached = []
        for dev in self.devices[0]:
            if type(dev) == TypeUSB:
                attached.append(dev.sn)
        self.device_sn = attached
        return self.device_sn

    def setBus3V3(self, state: bool):
        self._togglePin(self._bus_pin, state)

    def setPowerSwitch(self, state: bool):
        self._togglePin(self._pwr_pin, state)

    def stateCBus(self):
        if self.connected:
            return self._cbus_state
        return 0

    def stateRx(self):
        if self.connected:
            return self._serial.in_wating
        return 0

    def stateTx(self):
        if self.connected:
            return self._serial.out_waiting
        return 0

    def tx(self,data):
        if self.connected:
            ok = 0
            if type(data)==int:
                div, mod = divmod(int, 255)
                num_bytes = div + (mod > 0)
                data_bytes = data.to_bytes(num_bytes, byteorder="little")
                ok = 1
            if type(data)==str:
                data_bytes = data.encode('utf-8')
                ok = 1

            if ok:
                try:
                    self._write(data_bytes)
                    log.debug('Transmission successful: {}'.format(data_bytes))
                except Exception as e:
                    log.error(e)
            else:
                log.warning('Transmission unsuccessful: Use int or str, not {}.'.format(type(data)))
        else:
            log.warning('Connect to a device prior to transmitting data.')

    def _initCBus(self):
        pins = (1 << self._bus_pin) | ( 1 << self._pwr_pin)
        self._ftdi.set_cbus_direction(pins, pins)
        self._ftdi.set_cbus_gpio(self._cbus_init_state)

    def _read(self, n_bytes):
        return self._serial.read(n_bytes)

    def _togglePin(self, pin, state):
        if self.connected:
            mask = (1 << pin)
            if state == True:
                self._cbus_state = self._cbus_state | mask
            if state == False:
                self._cbus_state = self._cbus_state &~ mask
            self._ftdi.set_cbus_gpio(self._cbus_state)
        else:
            log.warning('Connect to a device prior to toggling IO.')

    def _write(self, data_bytes):
        self._serial.write(data_bytes)