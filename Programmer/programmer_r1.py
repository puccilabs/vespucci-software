import logging
from logging import NullHandler
from time import time
from programmer import Programmer

R1_BAUD_RATE = 115200
POWER_ON_DELAY = 750
POWER_ON_INPUT = 1250
POWER_OFF_INPUT = 1750
SYNC_CMD = 'AT'
SYNC_REPLY = 'OK'
SYNC_TIMEOUT = 500

log = logging.getLogger(__name__)
log.addHandler(NullHandler())

class Interface_R1(Programmer):

    def __init__(self):
        super().__init__()
        self.power_state = False
        self.sync = False
        self.connect(self.device_sn[0], R1_BAUD_RATE)

    def __del__(self):
        self.powerOn(False)
        super.__del__()

    def powerOn(self, power_state:bool):
        if power_state == True:
            time_start = time()*1000
            while(time()*1000 < time_start + POWER_ON_DELAY):
                pass
            self.setPowerSwitch(True)
            while(time()*1000 < time_start + POWER_ON_DELAY + POWER_ON_INPUT):
                pass
            self.setPowerSwitch(False)
            self.power_state = True

        if power_state == False:
            time_start = time()*1000
            self.setPowerSwitch(True)
            while(time()*1000 < time_start + POWER_OFF_INPUT):
                pass
            self.setPowerSwitch(False)
            self.power_state = False
            self.sync = False

    def syncSerial(self):
        if self.power_state == True:
            time_start = time()*1000
            waiting = True
            while(waiting & (time()*1000 < time_start + SYNC_TIMEOUT)):
                self.tx(SYNC_CMD)
                if self.stateRx():
                    reply = self.rx(2)
                    if reply == SYNC_REPLY:
                        waiting = False
                        self.sync = True
                    else:
                        log.warning('Expected `{i}`, received `{r}`'.format(i=SYNC_REPLY, r=reply))
            self.flushBuffers()
        else:
            log.warning('Power up R1 before syncing.')